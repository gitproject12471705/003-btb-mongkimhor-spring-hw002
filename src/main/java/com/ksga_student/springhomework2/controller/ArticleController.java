package com.ksga_student.springhomework2.controller;

import com.ksga_student.springhomework2.model.Article;
import com.ksga_student.springhomework2.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;


@Controller
public class ArticleController {
    @Value("${image.path}")
    private String serverPath;

    private final ArticleService articleService;

    @Autowired
    public ArticleController (ArticleService articleService){
        this.articleService = articleService;
    }

    @GetMapping("/")
    public String showAllArticles(ModelMap model){
        model.addAttribute("articles", articleService.findAll());
        return "index";
    }

    @GetMapping("/{id}/view")
    public String showArticle(@PathVariable("id") int id, ModelMap model){
        model.addAttribute("article", articleService.findById(id));
        return "article/show";
    }

    @GetMapping("/add-form")
    public String addForm(){
        return "article/form-add";
    }

    @PostMapping("/save-article")
    public String insertArticle(@ModelAttribute Article article, @RequestPart(name = "file") MultipartFile multipartFile) throws IOException {

        String originalFileName = multipartFile.getOriginalFilename();
        String afterSavedFileName = UUID.randomUUID() + "." + originalFileName.substring(originalFileName.lastIndexOf(".") + 1);

        if(!multipartFile.isEmpty()){
            Files.copy(multipartFile.getInputStream(), Paths.get(serverPath, afterSavedFileName));
        }

        article.setImage(afterSavedFileName);

        int id = articleService.findAll().size() + 1;

        article.setId(id);
        if(articleService.add(article))
            return "redirect:/";

        return "redirect:/add";
    }


    @GetMapping("/{id}/delete")
    public String deleteArticle(@PathVariable("id") int id){
        articleService.remove(id - 1);
        return "redirect:/";
    }


    @GetMapping("/{id}/edit")
    public String updateForm(@PathVariable("id") int id, ModelMap model){
        model.addAttribute("article", articleService.findById(id));
        return "article/edit";
    }

    @PostMapping("/{id}/update-article")
    public String updateArticle(@PathVariable("id") int id,
                                @RequestPart(name = "file") MultipartFile multipartFile,
                                @ModelAttribute Article article) throws IOException {

        String originalFileName = multipartFile.getOriginalFilename();
        String afterSavedFileName = UUID.randomUUID() + "." + originalFileName.substring(originalFileName.lastIndexOf(".") + 1);

        if(!multipartFile.isEmpty()){
            Files.copy(multipartFile.getInputStream(), Paths.get(serverPath, afterSavedFileName));
        }

        article.setImage(afterSavedFileName);
        articleService.edit(article);
        return "redirect:/";
    }


}
